package com.luis.ebitests.simplecache;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.io.IOException;
import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import com.luis.ebitests.simplecache.accesslayer.DiskLayer;
import com.luis.ebitests.simplecache.accesslayer.MemoryLayer;
import com.luis.ebitests.simplecache.accesslayer.SimpleCacheAccessLayer;
import com.luis.ebitests.simplecache.dummy.DummyClass;

public class TwoLevelCacheTest {

	private DiskLayer diskLayer;
	private MemoryLayer memLayer;
	private TwoLevelCache cache;
	
	//Are 0 because access are handled by mock
	private static int MEMORY_SIZE = 0;
	private static int DISK_SIZE = 0;
	
	@Before
	public void setUp(){
		diskLayer = mock(DiskLayer.class);
		memLayer = mock(MemoryLayer.class);
		
		cache = new TwoLevelCache(MEMORY_SIZE, DISK_SIZE);
	}
	
	@Test
	public void putMethodShouldAddAtAllLayers() throws IOException {
		//Arrange
		DummyClass dummy = new DummyClass(1, "Dummy!");
		String key = String.valueOf(dummy.hashCode());
		
		ArrayList<SimpleCacheAccessLayer> mockLayers = new ArrayList<SimpleCacheAccessLayer>(2);
		mockLayers.add(memLayer);
		mockLayers.add(diskLayer);
		cache.setLayers(mockLayers);
		//Act
		cache.put(key, dummy);
		//Assert
		verify(memLayer, times(1)).put(key, dummy);
		verify(diskLayer, times(1)).put(key, dummy);

	}
	
	@Test
	public void houldReturnNullWhenDoesNotExist() throws ClassNotFoundException, IOException{
		
		//Arrange
		String unexistentKey = "Idontexist";
		when(memLayer.get(unexistentKey)).thenReturn(null);
		when(diskLayer.get(unexistentKey)).thenReturn(null);
		
		ArrayList<SimpleCacheAccessLayer> mockLayers = new ArrayList<SimpleCacheAccessLayer>(2);
		mockLayers.add(memLayer);
		mockLayers.add(diskLayer);
		cache.setLayers(mockLayers);
		//Act
		Object result = cache.get(unexistentKey);
		//Assert
		verify(memLayer, times(1)).get(unexistentKey);
		verify(diskLayer, times(1)).get(unexistentKey);
		verify(memLayer, times(0)).put(any(String.class), any(Object.class));
		verify(diskLayer, times(0)).put(any(String.class), any(Object.class));

		assertEquals(null, result);
	}
	
	@Test
	public void shouldOnlyAccessMemoryLayerWhenIsFoundThere() throws ClassNotFoundException, IOException{
		String key = "key";
		DummyClass dummy = new DummyClass(1,"Dummy");
		
		when(memLayer.get(key)).thenReturn(dummy);
		
		ArrayList<SimpleCacheAccessLayer> mockLayers = new ArrayList<SimpleCacheAccessLayer>(2);
		mockLayers.add(memLayer);
		mockLayers.add(diskLayer);
		cache.setLayers(mockLayers);
		//Act
		Object result = cache.get(key);
		//Assert
		verify(memLayer, times(1)).get(key);
		verify(diskLayer, times(0)).get(key);
		verify(memLayer, times(0)).put(any(String.class), any(Object.class));
		verify(diskLayer, times(0)).put(any(String.class), any(Object.class));
		
		assertEquals(dummy, result);
	}
	
	@Test
	public void shouldAccessToFileLayerWhenIsNotFoundAtMemLayerUpdatingMemoryLayer() throws ClassNotFoundException, IOException{
		String key = "key";
		DummyClass dummy = new DummyClass(1,"Dummy");
		
		when(memLayer.get(key)).thenReturn(null);
		when(diskLayer.get(key)).thenReturn(dummy);

		
		ArrayList<SimpleCacheAccessLayer> mockLayers = new ArrayList<SimpleCacheAccessLayer>(2);
		mockLayers.add(memLayer);
		mockLayers.add(diskLayer);
		cache.setLayers(mockLayers);
		//Act
		Object result = cache.get(key);
		//Assert
		verify(memLayer, times(1)).get(key);
		verify(diskLayer, times(1)).get(key);
		verify(memLayer, times(1)).put(any(String.class), any(Object.class));
		verify(diskLayer, times(0)).put(any(String.class), any(Object.class));
		
		assertEquals(dummy, result);
	}

}
