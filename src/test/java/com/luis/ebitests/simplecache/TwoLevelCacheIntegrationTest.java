package com.luis.ebitests.simplecache;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import com.luis.ebitests.simplecache.accesslayer.DiskLayer;
import com.luis.ebitests.simplecache.accesslayer.FileHashMap;
import com.luis.ebitests.simplecache.accesslayer.SimpleCacheAccessLayer;
import com.luis.ebitests.simplecache.dummy.DummyClass;

public class TwoLevelCacheIntegrationTest {

	private int MEMORY_SIZE = 1;
	private int DISK_SIZE = 2;
	
	private static final String CACHE_PATH = "./test/resources/cache_integration";

	@Before
	public void setUp() throws IOException {
		delete(new File(CACHE_PATH));

		new File(CACHE_PATH).mkdirs();
	}
	@Test
	public void shouldExecuteGetOnEmptyCache() throws ClassNotFoundException, IOException {
		// Arrange
		String prefix = CACHE_PATH.concat(File.separator).concat("empty");
		new File(prefix).mkdirs();

		//Arrange
		String unexistentKey = "Idontexist";
		TwoLevelCache cache = new TwoLevelCache(MEMORY_SIZE, DISK_SIZE);

		
		ArrayList<SimpleCacheAccessLayer> layers = cache.getLayers();
		layers.set(1,  new DiskLayer(DISK_SIZE, prefix)) ;
		cache.setLayers(layers);

		//Act
		Object result = cache.get(unexistentKey);
		//Assert

		assertEquals(null, result);
	}

	@Test
	public void afterWrittingTwoTimesShouldBe1AtMemoryAndTwoAtFile() throws ClassNotFoundException, IOException {
		// Arrange
		String prefix = CACHE_PATH.concat(File.separator).concat("two_files");
		new File(prefix).mkdirs();

		//Arrange
		DummyClass dummy1 = new DummyClass(1, "dummy1");
		String key1 = String.valueOf(dummy1.hashCode());
		DummyClass dummy2 = new DummyClass(2, "dummy2");
		String key2 = String.valueOf(dummy2.hashCode());
		
		TwoLevelCache cache = new TwoLevelCache(MEMORY_SIZE, DISK_SIZE);

		
		ArrayList<SimpleCacheAccessLayer> layers = cache.getLayers();
		layers.set(1,  new DiskLayer(DISK_SIZE, prefix)) ;
		cache.setLayers(layers);

		//Act
		cache.put(key1, dummy1);
		cache.put(key2, dummy2);
		
		//Assert

		File dummy1File = new File(prefix+File.separator+key1);
		File dummy2File = new File(prefix+File.separator+key2);
		assertTrue(dummy1File.exists());
		assertTrue(dummy2File.exists());
		//Memory limit is 1, so last inserted is the stored at cache
		assertEquals(null, cache.getLayers().get(0).get(key1));
		assertEquals(dummy2, cache.getLayers().get(0).get(key2));
	}
	
	@Test
	public void afterWrittingTwoTimesShouldBe1AtMemoryLastChecked() throws ClassNotFoundException, IOException {
		// Arrange
		String prefix = CACHE_PATH.concat(File.separator).concat("two_files_b");
		new File(prefix).mkdirs();

		//Arrange
		DummyClass dummy1 = new DummyClass(1, "dummy1");
		String key1 = String.valueOf(dummy1.hashCode());
		DummyClass dummy2 = new DummyClass(2, "dummy2");
		String key2 = String.valueOf(dummy2.hashCode());
		
		TwoLevelCache cache = new TwoLevelCache(MEMORY_SIZE, DISK_SIZE);

		
		ArrayList<SimpleCacheAccessLayer> layers = cache.getLayers();
		layers.set(1,  new DiskLayer(DISK_SIZE, prefix)) ;
		cache.setLayers(layers);

		//Act
		cache.put(key1, dummy1);
		cache.put(key2, dummy2);
		Object retrieved = cache.get(key1);
		
		//Assert

		File dummy1File = new File(prefix+File.separator+key1);
		File dummy2File = new File(prefix+File.separator+key2);
		assertTrue(dummy1File.exists());
		assertTrue(dummy2File.exists());

		assertEquals(dummy1, cache.getLayers().get(0).get(key1));
		assertEquals(null, cache.getLayers().get(0).get(key2));
		assertEquals(dummy1, retrieved);
		
	}
	
	@Test
	public void afterWrittingThreeTimesShouldBe1AtMemoryLastCheckedAndTwoAtDisk() throws ClassNotFoundException, IOException, InterruptedException {
		// Arrange
		String prefix = CACHE_PATH.concat(File.separator).concat("two_files_c");
		new File(prefix).mkdirs();

		//Arrange
		DummyClass dummy1 = new DummyClass(1, "dummy1");
		String key1 = "dummy1";
		DummyClass dummy2 = new DummyClass(2, "dummy2");
		String key2 = "dummy2";
		DummyClass dummy3 = new DummyClass(3, "dummy3");
		String key3 = "dummy3";
		
		TwoLevelCache cache = new TwoLevelCache(MEMORY_SIZE, DISK_SIZE);

		
		ArrayList<SimpleCacheAccessLayer> layers = cache.getLayers();
		layers.set(1,  new DiskLayer(DISK_SIZE, prefix)) ;
		cache.setLayers(layers);

		//Act
		cache.put(key1, dummy1);
		Thread.sleep(500);
		cache.put(key2, dummy2);
		cache.put(key3, dummy3); //Overrides key1
		
		Object retrieved1 = cache.get(key1);
		Object retrieved2 = cache.get(key2);
		Object retrieved3 = cache.get(key3); 
		
		//Assert

		File dummy1File = new File(prefix+File.separator+key1);
		File dummy2File = new File(prefix+File.separator+key2);
		File dummy3File = new File(prefix+File.separator+key3);

		assertFalse(dummy1File.exists()); 
		assertTrue(dummy2File.exists());
		assertTrue(dummy3File.exists());

		assertEquals(null, cache.getLayers().get(0).get(key1));
		assertEquals(null, cache.getLayers().get(0).get(key2));
		assertEquals(dummy3, cache.getLayers().get(0).get(key3));
		
		assertEquals(null, retrieved1);
		assertEquals(dummy2, retrieved2);
		assertEquals(dummy3, retrieved3);
		
	}
	

	
	// From
	// http://stackoverflow.com/questions/779519/delete-directories-recursively-in-java
	private void delete(File f) throws IOException {
		if (f.isDirectory()) {
			for (File c : f.listFiles())
				delete(c);
		}
		if (!f.delete())
			throw new FileNotFoundException("Failed to delete file: " + f);
	}

}
