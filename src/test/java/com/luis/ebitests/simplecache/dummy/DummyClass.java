package com.luis.ebitests.simplecache.dummy;

import java.io.Serializable;

public class DummyClass implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int value1;
	private String value2;
	
	public DummyClass(int value1, String value2){
		this.value1 = value1;
		this.value2 = value2;
	}
	
	public int getValue1(){
		return this.value1;
	}
	
	public String getValue2(){
		return this.value2;
	}
	
	
	public boolean equals(Object other){
		if(other instanceof DummyClass == false) { 
			return false;
		}
		DummyClass otherDummy = (DummyClass) other;
		return (this.value1 == otherDummy.getValue1() && this.value2.equals(otherDummy.getValue2()));
	}
}
