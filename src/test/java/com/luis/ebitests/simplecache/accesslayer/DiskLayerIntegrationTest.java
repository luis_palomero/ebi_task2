package com.luis.ebitests.simplecache.accesslayer;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

import com.luis.ebitests.simplecache.dummy.DummyClass;

public class DiskLayerIntegrationTest {

	private int DISK_BASE_CAPACITY = 3;
	private static final String CACHE_PATH = "./test/resources/cache";

	@Before
	public void setUp() throws IOException {
		delete(new File(CACHE_PATH));

		new File(CACHE_PATH).mkdirs();
	}

	@Test
	public void shouldExecuteGetOnEmptyCache() {
		// Arrange
		String prefix = CACHE_PATH.concat(File.separator).concat("empty");
		new File(prefix).mkdirs();

		FileHashMap fileHashMap = new FileHashMap(prefix, DISK_BASE_CAPACITY);
		DiskLayer layer = new DiskLayer(DISK_BASE_CAPACITY, fileHashMap);
		DummyClass dummy = new DummyClass(1, "Dummy I am");
		String key = String.valueOf(dummy.hashCode());
		Object returned = null;
		// Act
		try {
			returned = layer.get(key);
		} catch (Exception e) {
			fail("Exception occured fetching value from disk cache [".concat(e.toString()).concat("]"));
		}

		// Assert
		assertEquals(null, returned);
		assertEquals(0, layer.size());
	}

	@Test
	public void shouldStoreAndRetrieveItemInEmptyCache() {
		// Arrange
		String prefix = CACHE_PATH.concat(File.separator).concat("one");
		new File(prefix).mkdirs();

		FileHashMap fileHashMap = new FileHashMap(prefix, DISK_BASE_CAPACITY);
		DiskLayer layer = new DiskLayer(DISK_BASE_CAPACITY, fileHashMap);
		DummyClass dummy = new DummyClass(1, "Dummy I am");
		String key = String.valueOf(dummy.hashCode());
		Object returned = null;
		// Act
		try {
			layer.put(key, dummy);
			returned = (DummyClass) layer.get(key);
		} catch (Exception e) {
			fail("Exception occured fetching value from disk cache [".concat(e.toString()).concat("]"));
		}

		// Assert
		assertEquals(dummy, returned);
		assertEquals(1, layer.size());
	}

	@Test
	public void shouldDeleteOldesWhenOverflow(){		
		String prefix = CACHE_PATH.concat(File.separator).concat("many");
		new File(prefix).mkdirs();

		FileHashMap fileHashMap = new FileHashMap(prefix, DISK_BASE_CAPACITY);
		DiskLayer layer = new DiskLayer(DISK_BASE_CAPACITY, fileHashMap);
		DummyClass dummy1 = new DummyClass(1, "Dummy I am");
		DummyClass dummy2 = new DummyClass(2, "Dummy dummy I am");
		DummyClass dummy3 = new DummyClass(3, "Dummy dummy dummy I am");
		DummyClass dummy4 = new DummyClass(4, "Dummy dummy dummy dummy I am");
		
		String key1 = String.valueOf(dummy1.hashCode());
		String key2 = String.valueOf(dummy2.hashCode());
		String key3 = String.valueOf(dummy3.hashCode());
		String key4 = String.valueOf(dummy4.hashCode());
		
		Object returned1 = null;
		Object returned2 = null;
		Object returned3 = null;
		Object returned4 = null;		
		// Act
		try {
			layer.put(key1, dummy1);
			Thread.sleep(600);
			layer.put(key2, dummy2);
			layer.put(key3, dummy3);
			layer.put(key4, dummy4);
			returned1 = (DummyClass) layer.get(key1);
			returned2 = (DummyClass) layer.get(key2);
			returned3 = (DummyClass) layer.get(key3);
			returned4 = (DummyClass) layer.get(key4);
		} catch (Exception e) {
			fail("Exception occured fetching value from disk cache [".concat(e.toString()).concat("]"));
		}

		// Assert
		assertEquals(null, returned1);
		assertEquals(dummy2, returned2);
		assertEquals(dummy3, returned3);
		assertEquals(dummy4, returned4);
		assertEquals(3, layer.size());
	}
	
	@Test
	public void shouldDeleteOldestWhenOverflowAndMixedPuts(){
		String prefix = CACHE_PATH.concat(File.separator).concat("many");
		new File(prefix).mkdirs();

		FileHashMap fileHashMap = new FileHashMap(prefix, DISK_BASE_CAPACITY);
		DiskLayer layer = new DiskLayer(DISK_BASE_CAPACITY, fileHashMap);
		DummyClass dummy1 = new DummyClass(1, "Dummy I am");
		DummyClass dummy2 = new DummyClass(2, "Dummy dummy I am");
		DummyClass dummy3 = new DummyClass(3, "Dummy dummy dummy I am");
		DummyClass dummy4 = new DummyClass(4, "Dummy dummy dummy dummy I am");
		
		String key1 = String.valueOf(dummy1.hashCode());
		String key2 = String.valueOf(dummy2.hashCode());
		String key3 = String.valueOf(dummy3.hashCode());
		String key4 = String.valueOf(dummy4.hashCode());
		
		Object returned1 = null;
		Object returned2 = null;
		Object returned3 = null;
		Object returned4 = null;		
		// Act
		try {
			layer.put(key1, dummy1);			
			layer.put(key2, dummy2);			
			layer.put(key3, dummy3);
			Thread.sleep(400);
			layer.put(key1, dummy1);
			layer.put(key2, dummy2);			
			layer.put(key4, dummy4);		
			returned1 = (DummyClass) layer.get(key1);
			returned2 = (DummyClass) layer.get(key2);
			returned3 = (DummyClass) layer.get(key3);
			returned4 = (DummyClass) layer.get(key4);
		} catch (Exception e) {
			fail("Exception occured fetching value from disk cache [".concat(e.toString()).concat("]"));
		}

		// Assert
		assertEquals(dummy1, returned1);
		assertEquals(dummy2, returned2);
		//With different puts he is now the last
		assertEquals(null, returned3);
		assertEquals(dummy4, returned4);
		assertEquals(3, layer.size());
	}
	
	
	//From http://stackoverflow.com/questions/779519/delete-directories-recursively-in-java
	private void delete(File f) throws IOException {
		if (f.isDirectory()) {
			for (File c : f.listFiles())
				delete(c);
		}
		if (!f.delete())
			throw new FileNotFoundException("Failed to delete file: " + f);
	}
}
