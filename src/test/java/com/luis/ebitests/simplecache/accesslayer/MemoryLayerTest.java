package com.luis.ebitests.simplecache.accesslayer;

import static org.junit.Assert.*;

import org.junit.Test;

import com.luis.ebitests.simplecache.dummy.DummyClass;

public class MemoryLayerTest {

	private static int BASE_MEMORY_CAPACITY = 3;

	@Test
	public void builderShouldInitializeEmptyObjects() {
		// Arrange

		// Act
		MemoryLayer layer = new MemoryLayer(BASE_MEMORY_CAPACITY);

		// Assert
		assertEquals(BASE_MEMORY_CAPACITY, layer.maxSize());
		assertEquals(0, layer.size());
	}

	@Test
	public void EachLayerShouldKeepItsSize() {
		// Arrange

		// Act
		MemoryLayer layer1 = new MemoryLayer(BASE_MEMORY_CAPACITY);
		MemoryLayer layer2 = new MemoryLayer(BASE_MEMORY_CAPACITY * 2);

		// Assert
		assertEquals(BASE_MEMORY_CAPACITY, layer1.maxSize());
		assertEquals(BASE_MEMORY_CAPACITY * 2, layer2.maxSize());

	}

	@Test
	public void shouldNotFindValuesWithEmptyArray() {
		// Arrange
		Object retrieved;
		String key = "dummy1";
		MemoryLayer layer = new MemoryLayer(BASE_MEMORY_CAPACITY);

		// Act
		retrieved = layer.get(key);

		// Assert
		assertEquals(null, retrieved);
		assertEquals(0, layer.size());
	}

	@Test
	public void shouldInsertNewValueWhenDoesNotExist() {
		// Arrange
		Object dummy = new DummyClass(10, "Dummy");
		String key = "dummy1";
		MemoryLayer layer = new MemoryLayer(BASE_MEMORY_CAPACITY);

		// Act
		layer.put(key, dummy);

		// Assert
		assertEquals(1, layer.size());
	}

	@Test
	public void shouldRetrieveExistentValue() {
		// Arrange
		Object dummy = new DummyClass(10, "Dummy");
		String key = "dummy1";
		MemoryLayer layer = new MemoryLayer(BASE_MEMORY_CAPACITY);
		Object retrieved = null;

		// Act
		layer.put(key, dummy);
		retrieved = layer.get(key);

		// Assert
		assertEquals(dummy, retrieved);

	}

	@Test
	public void shouldOverrideExistentValue() {
		// Arrange
		Object dummy1 = new DummyClass(10, "Dummy");
		Object dummy2 = new DummyClass(10, "Other Dummy");
		String key = "dummy1";
		MemoryLayer layer = new MemoryLayer(BASE_MEMORY_CAPACITY);
		Object retrieved = null;

		// Act
		layer.put(key, dummy1);
		layer.put(key, dummy2);
		retrieved = layer.get(key);

		// Assert
		assertEquals(dummy2, retrieved);
		assertEquals(1, layer.size());

	}

	@Test
	public void shouldInsertNewObjectAtNewKey() {
		// Arrange
		Object dummy1 = new DummyClass(10, "Dummy");
		Object dummy2 = new DummyClass(10, "Other Dummy");
		String key1 = "dummy1";
		String key2 = "dummy2";
		MemoryLayer layer = new MemoryLayer(BASE_MEMORY_CAPACITY);
		Object retrieved1 = null;
		Object retrieved2 = null;

		// Act
		layer.put(key1, dummy1);
		layer.put(key2, dummy2);
		retrieved1 = layer.get(key1);
		retrieved2 = layer.get(key2);

		// Assert
		assertEquals(dummy1, retrieved1);
		assertEquals(dummy2, retrieved2);
		assertEquals(2, layer.size());
	}

	@Test
	public void shouldDeleteOldKeysWhenAreMorePutsThanBaseSize() {
		// Arrange
		Object dummy1 = new DummyClass(10, "Dummy");
		Object dummy2 = new DummyClass(10, "Other Dummy");
		Object dummy3 = new DummyClass(10, "Other other Dummy");
		Object dummy4 = new DummyClass(10, "Other other other Dummy");
		String key1 = "a";
		String key2 = "b";
		String key3 = "c";
		String key4 = "d";

		MemoryLayer layer = new MemoryLayer(BASE_MEMORY_CAPACITY);
		Object retrieved1 = null;
		Object retrieved2 = null;
		Object retrieved3 = null;
		Object retrieved4 = null;

		// Act
		layer.put(key1, dummy1);
		layer.put(key2, dummy2);
		layer.put(key3, dummy3);
		layer.put(key4, dummy4);

		retrieved1 = layer.get(key1);
		retrieved2 = layer.get(key2);
		retrieved3 = layer.get(key3);
		retrieved4 = layer.get(key4);

		// Assert
		assertEquals(null, retrieved1);
		assertEquals(dummy2, retrieved2);
		assertEquals(dummy3, retrieved3);
		assertEquals(dummy4, retrieved4);
		assertEquals(3, layer.size());
	}

	@Test
	public void shouldDeleteOldKeysWhenAreMorePutsThanBaseSizeMixingPuts() {
		// Arrange
		Object dummy1 = new DummyClass(10, "Dummy");
		Object dummy2 = new DummyClass(10, "Other Dummy");
		Object dummy3 = new DummyClass(10, "Other other Dummy");
		Object dummy4 = new DummyClass(10, "Other other other Dummy");
		String key1 = "a";
		String key2 = "b";
		String key3 = "c";
		String key4 = "d";

		MemoryLayer layer = new MemoryLayer(BASE_MEMORY_CAPACITY);
		Object retrieved1 = null;
		Object retrieved2 = null;
		Object retrieved3 = null;
		Object retrieved4 = null;

		// Act
		layer.put(key1, dummy1);
		layer.put(key2, dummy2);
		layer.put(key3, dummy3);
		layer.put(key2, dummy2);
		layer.put(key4, dummy4);
		layer.put(key3, dummy3);
		layer.put(key1, dummy1);

		retrieved1 = layer.get(key1);
		retrieved2 = layer.get(key2);
		retrieved3 = layer.get(key3);
		retrieved4 = layer.get(key4);

		// Assert
		assertEquals(dummy1, retrieved1);
		assertEquals(null, retrieved2);
		assertEquals(dummy3, retrieved3);
		assertEquals(dummy4, retrieved4);
		assertEquals(3, layer.size());
	}
}
