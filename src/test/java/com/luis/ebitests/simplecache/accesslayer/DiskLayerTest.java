package com.luis.ebitests.simplecache.accesslayer;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

import com.luis.ebitests.simplecache.dummy.DummyClass;

public class DiskLayerTest {
	
	private static int BASE_DISK_CAPACITY = 3;	
	private Object dummy;
	
	@Before
	public void setUp(){
		dummy = new DummyClass(10, "I am dummy");
	}
	
	@Test
	public void builderShouldInitializeEmptyObjects() {
		// Arrange
		FileHashMap mockFileHashMap = mock(FileHashMap.class);
		
		// Act
		DiskLayer layer = new DiskLayer(BASE_DISK_CAPACITY, mockFileHashMap);
		layer.size();
		// Assert		
		assertEquals(BASE_DISK_CAPACITY, layer.maxSize());
		verify(mockFileHashMap, times(1)).size();

	}
	
	@Test
	public void shouldAccessFileSystemForRetrieveKeys() throws IOException, ClassNotFoundException {

		// Arrange
		Object retrieved1 = null;
		Object retrieved2 = null;
		String key = "dummy1";
		
		FileHashMap mockFileHashMap = mock(FileHashMap.class);
		
		when(mockFileHashMap.get(key)).thenReturn(null).thenReturn(dummy);
		when(mockFileHashMap.size()).thenReturn(1);
		
		DiskLayer layer = new DiskLayer(BASE_DISK_CAPACITY, mockFileHashMap);

		// Act
		retrieved1 = layer.get(key);
		retrieved2 = layer.get(key);

		// Assert
		assertEquals(null, retrieved1);
		assertEquals(dummy, retrieved2);
		assertEquals(1, layer.size());
	}
	
	@Test
	public void shouldAccessFileSystemForStoreKeys() throws IOException{
		//Arrange
		String key = "dummy1";
		FileHashMap mockFileHashMap = mock(FileHashMap.class);
		DiskLayer layer = new DiskLayer(BASE_DISK_CAPACITY, mockFileHashMap);

		//Act
		layer.put(key, dummy);
		
		//Assert
		verify(mockFileHashMap, times(1)).put(key, dummy);

	}
}
