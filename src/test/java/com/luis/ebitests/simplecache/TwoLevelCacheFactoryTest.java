package com.luis.ebitests.simplecache;

import static org.junit.Assert.*;

import org.junit.Test;

import com.luis.ebitests.simplecache.accesslayer.DiskLayer;
import com.luis.ebitests.simplecache.accesslayer.MemoryLayer;
import com.luis.ebitests.simplecache.accesslayer.SimpleCacheAccessLayer;

public class TwoLevelCacheFactoryTest {
	
	private static int SAMPLE_DISK_CAPACITY = 4;
	private static int SAMPLE_MEMORY_CAPACITY = 2;
	private static int LAYERS_DEFINED = 2;
	
	@Test
	public void shouldCreateInstanceOnCall(){		
		
		//Arrange
		SimpleCacheFactory factory = new TwoLevelCacheFactory();
				
		//Act
		SimpleCache cache =  factory.createCache(SAMPLE_MEMORY_CAPACITY, SAMPLE_DISK_CAPACITY);
		TwoLevelCache twoLevelCache = (TwoLevelCache)cache;
		
		//Assert
		assertEquals(SAMPLE_MEMORY_CAPACITY,twoLevelCache.getMemoryCapacity());
		assertEquals(SAMPLE_DISK_CAPACITY,twoLevelCache.getDiskCapacity());

		assertEquals(LAYERS_DEFINED, twoLevelCache.getLayers().size());
		assertTrue(twoLevelCache.getLayers().get(0) instanceof MemoryLayer);
		assertTrue(twoLevelCache.getLayers().get(1) instanceof DiskLayer);
		
	}
	
	
}
