package com.luis.ebitests.simplecache;

public interface SimpleCacheFactory {
	public SimpleCache createCache(int memoryCapacity, int diskCapacity);
}
