package com.luis.ebitests.simplecache;

import java.io.IOException;

public interface SimpleCache {

	public void put(String key, Object value) throws IOException;

	public Object get(String key) throws IOException, ClassNotFoundException;
}