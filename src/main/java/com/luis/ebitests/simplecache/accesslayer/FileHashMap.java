package com.luis.ebitests.simplecache.accesslayer;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class FileHashMap {

	private String folder;
	private int capacity;
	
	public FileHashMap(String folder, int capacity) {
		this.folder = folder;
		this.capacity = capacity;
	}

	public void put(String key, Object value) throws IOException {
		String fileName = this.generateFileName(key);
		this.write(fileName, value);
		if(removeEldestEntry() == true){
			File eldest = this.findEldest();
			eldest.delete();
		}

	}

	public Object get(String key) throws IOException, ClassNotFoundException {		
		InputStream file;
		try {
			file = new FileInputStream(this.generateFileName(key));
		} catch (FileNotFoundException e) {
			return null;
		}
		InputStream buffer = new BufferedInputStream(file);
		ObjectInputStream in = new ObjectInputStream(buffer);
		return (Object)in.readObject();
	}
	
	public int size(){		
		return this.fetchAll().length;
	}

	protected boolean removeEldestEntry() {
		return this.size() > this.capacity;
	}

	private String generateFileName(String key) {
		return this.folder.concat(File.separator).concat(key);
	}
	
	private void write(String fileName, Object value) throws IOException{
		FileOutputStream fileOut = new FileOutputStream(fileName);
        ObjectOutputStream out = new ObjectOutputStream(fileOut);
        out.writeObject(value);
        out.flush();
        out.close();
        fileOut.close();
	}
	
	private File[] fetchAll(){
	    File fl = new File(this.folder);
	    File[] files = fl.listFiles(new FileFilter() {	    	
	        public boolean accept(File file) {
	            return file.isFile();
	        }
	    });
	    return files;
	}
	
	private File findEldest(){	   
	    File[] files = this.fetchAll();
	    
	    long lastMod = Long.MAX_VALUE;
	    File choice = null;
	    for (File file : files) {
	        if (file.lastModified() < lastMod) {
	            choice = file;
	            lastMod = file.lastModified();
	        }
	    }
	    return choice;
	}

}
