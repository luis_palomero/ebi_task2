package com.luis.ebitests.simplecache.accesslayer;

import java.io.IOException;

public interface SimpleCacheAccessLayer {
	public void put(String key, Object value) throws IOException;

	public Object get(String key) throws IOException, ClassNotFoundException;
}
