package com.luis.ebitests.simplecache.accesslayer;

import java.io.IOException;

public class DiskLayer implements SimpleCacheAccessLayer {

	private int maxSize;
	private FileHashMap fileHashMap;
	
	public DiskLayer(int capacity) {
		this.maxSize = capacity;
		this.fileHashMap = new FileHashMap("./.cache", capacity);
	}

	public DiskLayer(int capacity, FileHashMap fileHashMap){
		this.maxSize = capacity;
		this.fileHashMap = fileHashMap;
	}

	public DiskLayer(int capacity, String prefix){
		this.maxSize = capacity;
		this.fileHashMap = new FileHashMap(prefix, capacity);
	}
		
	public int size(){
		return this.fileHashMap.size();
	}
	
	public int maxSize(){
		return this.maxSize;
	}

	public void put(String key, Object value) throws IOException {
		this.fileHashMap.put(key, value);
	}

	public Object get(String key) throws IOException, ClassNotFoundException {
		return this.fileHashMap.get(key);
	}
}
