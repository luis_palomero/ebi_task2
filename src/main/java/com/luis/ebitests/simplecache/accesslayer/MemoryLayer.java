package com.luis.ebitests.simplecache.accesslayer;

import java.util.LinkedHashMap;
import java.util.Map;

public class MemoryLayer implements SimpleCacheAccessLayer {

	private int maxEntries;
	private LinkedHashMap<String, Object> items;


	public MemoryLayer(final int capacity) {
		this.maxEntries = capacity;
		this.items = new LinkedHashMap<String, Object>(capacity, .75F, false){
			protected boolean removeEldestEntry(Map.Entry eldest){
			    return size() > capacity;
			}
		};
	}

	public int maxSize() {
		return this.maxEntries;
	}

	public int size() {
		return this.items.size();
	}

	public void put(String key, Object value) {
		this.items.put(key, value);

	}

	public Object get(String key) {
		return this.items.get(key);
	}

}
