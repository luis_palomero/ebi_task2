package com.luis.ebitests.simplecache;

public class TwoLevelCacheFactory implements SimpleCacheFactory {

	public SimpleCache createCache(int memoryCapacity, int diskCapacity) {

		return new TwoLevelCache(memoryCapacity, diskCapacity);
	}

}
