package com.luis.ebitests.simplecache;

import java.io.IOException;
import java.util.ArrayList;

import com.luis.ebitests.simplecache.accesslayer.DiskLayer;
import com.luis.ebitests.simplecache.accesslayer.MemoryLayer;
import com.luis.ebitests.simplecache.accesslayer.SimpleCacheAccessLayer;

public class TwoLevelCache implements SimpleCache {

	private int memoryCapacity;
	private int diskCapacity;

	private ArrayList<SimpleCacheAccessLayer> layers;

	public TwoLevelCache(int memoryCapacity, int diskCapacity) {
		this.memoryCapacity = memoryCapacity;
		this.diskCapacity = diskCapacity;
		this.init();
	}

	public void init() {
		this.layers = new ArrayList<SimpleCacheAccessLayer>(2);
		this.layers.add(new MemoryLayer(this.memoryCapacity));
		this.layers.add(new DiskLayer(this.diskCapacity));
	}

	public int getMemoryCapacity() {
		return this.memoryCapacity;
	}

	public int getDiskCapacity() {
		return this.diskCapacity;
	}

	public void setLayers(ArrayList<SimpleCacheAccessLayer> layers) {
		this.layers = layers;
	}

	public ArrayList<SimpleCacheAccessLayer> getLayers() {
		return this.layers;
	}

	public void put(String key, Object value) throws IOException {
		for (SimpleCacheAccessLayer layer : this.layers) {
			layer.put(key, value);
		}

	}

	public Object get(String key) throws ClassNotFoundException, IOException {
		return this.fetchAtLayer(key, 0);
	}

	private Object fetchAtLayer(String key, int currLayer) throws ClassNotFoundException, IOException {
		Object candidate = this.layers.get(currLayer).get(key);
		if (candidate != null || currLayer >= this.layers.size() - 1) {
			return candidate;
		} else {
			candidate = this.fetchAtLayer(key, currLayer+1);
			if(candidate != null){
				this.layers.get(currLayer).put(key, candidate);
			}
			return candidate;
		}
	}

}
