Implement a 2-level cache using the following interfaces without using any existing caching libraries. Optimally, JUnit tests would be provided in addition to the implementation.


```
#!java

public interface
SimpleCacheFactory
{
    public SimpleCache createCache(int memoryCapacity, int diskCapacity);
}

public interface
SimpleCache
{
    public void put(Object key, Object value);
    public Object get(Object key);
}

```

- The 'memoryCapacity' is the maximum number of objects stored in memory.
- The 'diskCapacity' is the maximum number of objects stored on disk.
- Objects are stored preferentially in memory.
- If 'memoryCapacity' is exceeded then the oldest objects in memory should be moved to disk.
- If 'diskCapacity' is exceeded then the oldest objects on disk should be removed from the cache.
- When an object is fetched from disk it should be moved to memory cache.
- We leave the definition of 'oldest' to the implementer. 